import BowlingGame from '../../src/application/BowlingGame';

describe('BowlingGame', () => {
  describe('calculation', () => {
    xtest('初期値は0', () => {
      const target = new BowlingGame();

      expect(target.scoreResult).toEqual(0);
    });

    xtest('スコアを10に変更', () => {
      const target = new BowlingGame(10);

      target.score();

      expect(target.scoreResult).toEqual(10);
    });

    test('配列を引数として渡す', () => {
      const target = new BowlingGame([0, 5]);

      expect(target.scoreArray).toEqual([0, 5]);
    });

    test('複数の配列を引数として渡し、合計値を返す', () => {
      const target = new BowlingGame([[0, 5], [4, 2], [2, 7]]);

      expect(target.score()).toEqual(20);
    });
    test('ストライク、スペアのときに追加点', () => {
      const target = new BowlingGame([[10], [0, 5], [4, 2], [3, 7], [4, 5]]);

      expect(target.score()).toEqual(49);
    });
    test('パーフェクトで300点', () => {
      const target = new BowlingGame([[10], [10], [10], [10], [10],
        [10], [10], [10], [10], [10, 10, 10]]);

      expect(target.score()).toEqual(300);
    });
    test('本番', () => {
      const bowlingGame = new BowlingGame([
        [10],
        [8, 2],
        [4, 4],
        [3, 6],
        [3, 1],
        [5, 4],
        [3, 7],
        [2, 8],
        [10],
        [10, 10, 10],
      ]);
      expect(bowlingGame.score()).toEqual(156);
      console.log(bowlingGame.score());
    });
  });
});
