export default class BowlingGame {
  constructor(scoreArray) {
    this.scoreArray = scoreArray;
  }

  score() {
    let scoreResult = 0;
    let bonusCount = 0;
    this.scoreArray.forEach((flameScore) => {
      flameScore.forEach((score) => {
        scoreResult += score;
        if (bonusCount >= 3) {
          scoreResult += score * 2;
          bonusCount -= 2;
        } else if (bonusCount > 0) {
          scoreResult += score;
          bonusCount -= 1;
        }
      });
      if (flameScore[0] === 10) {
        bonusCount += 2;
      } else if (flameScore[0] + flameScore[1] === 10) {
        bonusCount += 1;
      }
    });
    return scoreResult;
  }
}
